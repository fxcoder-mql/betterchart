/*
Copyright 2023 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// BetterChart. Alternative Zoom In Key. © FXcoder

#property strict

#include "bsl/all.mqh"
#include "enum/mtkey.mqh"
#include "betterchartmodule.mqh"

class CAlternativeZoomInKey: public CBetterChartModule
{
public:

	virtual void chart_event() override
	{
#ifdef __MQL4__

		if (!_chartevent.is_key_down_event(MTKEY_EQUAL) ||
			_terminal.is_control_key_pressed() ||
			_terminal.is_shift_key_pressed())
			return;

		int scale = _chart.scale();

		// 5 is the max scale
		if (scale >= 5)
			return;

		// See fxcoder:mki#42

		int last_bar = _chart.last_visible_bar();
		_chart.scale(scale + 1);

		// The only way to get the chart's shift is to move it to the rightmost position and get the last visible bar.
		// This method is different from the original, but it is fairly close. I don’t know how MT really works here.
		_chart.navigate_end(0);
		int start_shift = _chart.last_visible_bar(); // negative

		int fixed_shift = _chart.autoscroll() ? 0 : _math.round_to_int(_chart.width_in_bars() * (1.0 - _chart.fixed_position() / 100.0));
		_chart.navigate_end(start_shift - fixed_shift - last_bar);
		_chart.redraw();

#endif

	}
};
