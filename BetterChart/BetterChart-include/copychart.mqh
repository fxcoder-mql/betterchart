/*
Copyright 2023 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// Better Chart. Copy Chart. © FXcoder

#property strict

#include "bsl/all.mqh"
#include "betterchartmodule.mqh"

class CCopyChart: public CBetterChartModule
{
private:

	string copy_template_name_;
	ushort key_;


public:

	void CCopyChart(ushort key):
		CBetterChartModule(),
		key_(key)
	{
#ifdef __MQL4__
		// mt4 does not support folders in templates
		copy_template_name_ = "~lastcopy.tpl";
#else
		copy_template_name_ = "betterchart/~lastcopy.tpl";
#endif
	}

	virtual void chart_event() override
	{
		if (!_chartevent.is_key_down_event(key_) ||
			!_terminal.is_shift_key_pressed() ||
			_terminal.is_control_key_pressed())
			return;

		ChartSaveTemplate(0, copy_template_name_);
		long chart_id = ChartOpen(_Symbol, _Period);
		ChartApplyTemplate(chart_id, copy_template_name_);
	}
};
