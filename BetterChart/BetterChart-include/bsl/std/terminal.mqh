/*
Copyright 2023 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// Terminal. BSL+E. © FXcoder

#property strict

class CBTerminal
{
public:

	// keys
	static bool is_shift_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_SHIFT    )); }
	static bool is_control_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_CONTROL  )); }


protected:

	// Check if the key is pressed (not held down like *Lock keys, they sould be checked on the low bit)
#ifdef __MQL4__
	static bool is_key_pressed(ENUM_TERMINAL_INFO_INTEGER key) { return((::TerminalInfoInteger(key) & 0x80) != 0); }
#else
	static bool is_key_pressed(ENUM_TERMINAL_INFO_INTEGER key) { return((::TerminalInfoInteger(key) & 0x8000) != 0); }
#endif

} _terminal;
