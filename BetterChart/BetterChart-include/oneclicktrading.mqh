/*
Copyright 2023 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// BetterChart. One Click Trade Key. © FXcoder

#property strict

#include "bsl/all.mqh"
#include "betterchartmodule.mqh"

class COneClickTradingKey: public CBetterChartModule
{
private:

	ushort key_;


public:

	void COneClickTradingKey(ushort key):
		CBetterChartModule(),
		key_(key)
	{
	}

	virtual void chart_event() override
	{
		if (!_chartevent.is_key_down_event(key_) ||
			_terminal.is_control_key_pressed() ||
			_terminal.is_shift_key_pressed())
			return;

		_chart.show_one_click(!_chart.show_one_click());
		_chart.redraw();
	}
};
