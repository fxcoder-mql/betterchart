/*
Copyright 2023 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "BetterChart 2.0. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property strict
#property indicator_chart_window
#property indicator_plots 0


#include "BetterChart-include/bsl/all.mqh"
#include "BetterChart-include/enum/mtkey.mqh"
#include "BetterChart-include/smartautoscroll.mqh"
#include "BetterChart-include/altzoomin.mqh"
#include "BetterChart-include/oneclicktrading.mqh"
#include "BetterChart-include/copychart.mqh"
#include "BetterChart-include/altvshift.mqh"


input bool       SmartAutoscroll       = true;               // Smart Autoscroll

#ifdef __MQL4__
input bool       AlternativeZoomInKey  = true;               // Alternative Zoom In Key (=)
#else
      bool       AlternativeZoomInKey  = false;
#endif

input ENUM_MTKEY OneClickTradingKey    = MTKEY_GRAVE_ACCENT; // One Click Trading Panel Key (None = disable)

input ENUM_MTKEY CopyChartKey          = MTKEY_D;            // Copy Chart Key (with Shift key, None = disable)
input bool       AlternativeVShift     = true;               // Alternative Vertical Shift


CBetterChartModule* modules_[];


void OnInit()
{
	if (SmartAutoscroll)
		_arr.add(modules_, new CSmartAutoscroll());

	if (AlternativeZoomInKey)
		_arr.add(modules_, new CAlternativeZoomInKey());

	if (OneClickTradingKey != MTKEY_NONE)
		_arr.add(modules_, new COneClickTradingKey(OneClickTradingKey));

	if (CopyChartKey != MTKEY_HOME)
		_arr.add(modules_, new CCopyChart(CopyChartKey));

	if (AlternativeVShift)
		_arr.add(modules_, new CAltVShift());
}

void OnDeinit(const int reason)
{
	_ptr.safe_delete_array(modules_);
}

void OnChartEvent(const int id, const long &lparam, const double &dparam, const string &sparam)
{
	if (!_tf.is_enabled())
		return;

	_chartevent.update(id, lparam, dparam, sparam);

	for (int i = ArraySize(modules_) - 1; i >= 0; --i)
		modules_[i].chart_event();
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	_tf.enable();
	return(rates_total);
}

/*
Changes:

2.0:
	* дублирование графика, #15
	* альтернативный вертикальный сдвиг курсором, #9

1.1:
	* кнопка для показа/скрытия панели торговли одним кликом, #51
	* кнопка = для увеличения масштаба в MT4 (в 5 это работает стандартно), #52
*/
